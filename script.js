function rand(m1,m2){
	return Math.floor(Math.random() * (m2 - m1 + 1)) + m1;
}

var paused=false;
var const_width = 512;
var const_height = 480;

//canvas init
var canvas=document.createElement("canvas");
var ctx=canvas.getContext('2d');
canvas.width=const_width;
canvas.height=const_height;
$(canvas).css({position:"absolute",top:0,left:0,"z-index":1});
document.body.appendChild(canvas);

//canvas bg init
var canvasbg=document.createElement("canvas");
var ctxbg=canvasbg.getContext('2d');
canvasbg.width=const_width;
canvasbg.height=const_height;
$(canvasbg).css({position:"absolute",top:0,left:0,"z-index":0});
document.body.appendChild(canvasbg);

//canvas foreground init
var canvasfg=document.createElement("canvas");
var ctxfg=canvasfg.getContext('2d');
canvasfg.width=const_width;
canvasfg.height=const_height;
$(canvasfg).css({position:"absolute",top:0,left:0,"z-index":2}).attr('id','cfg');
document.body.appendChild(canvasfg);

//draw background image onto background canvas
var bgReady=false;
var bgImage=new Image();
bgImage.onload=function(){
    bgReady=true;
    ctxbg.drawImage(bgImage,0,0);
};
bgImage.src="http://i59.tinypic.com/iy0kyh.png";

//heroimage init
var heroReady=false;
var heroImage=new Image();
heroImage.onload=function(){
    heroReady=true;
};
heroImage.src="http://i57.tinypic.com/jjbiu1.png";
var heroReadyR=false;
var heroImageR=new Image();
heroImageR.onload=function(){
    heroReadyR=true;
};
heroImageR.src="http://i66.tinypic.com/28j8osi.png";



//monsterimage initialization
var monsterReady=false;
var monsterImage=new Image();
monsterImage.onload=function(){
    monsterReady=true;
};
monsterImage.src="http://i62.tinypic.com/kbeweu.png";
var monsterReadyR=false;
var monsterImageR=new Image();
monsterImageR.onload=function(){
    monsterReadyR=true;
};
monsterImageR.src="http://oi59.tinypic.com/2j3jwb6.jpg";

//static variables, or levers to balance game
var hero={
    friction:0.88,
    accel:30,
    speedx:0,
    speedy:0,
    x: 0,
    y: 0,
    size:20
};

//Array for all of the monsters
var monsters={};

//keypress stuffs
var kd={
    38: false,
    40: false,
    37: false,
    39: false
};
$(document).keydown(function(e){
    kd[e.keyCode]=true;
});
$(document).keyup(function(e){
    kd[e.keyCode]=false;
    if(e.keyCode==77){
        console.log(monsters);   
    }else if(e.keyCode==66){
        hero.size+=10;
    }
});

var createMonster=function(){
    var i=0;
    while(monsters[i]!=undefined){
        i++;
    }
    var rol=rand(1,2);
    var siz=rand(10,hero.size+30);
    monsters[i]={
        speed: rand(40,120)*(rol==1?-1:1),
        x: rol==1?const_width:-50,
        y: rand(0,const_width-siz),
        size: siz
    };
};
var reset=function(){
    hero.x=canvas.width/2;
    hero.y=canvas.height/2;
    monsters={};
    hero.size=20;
    ctxfg.beginPath();
    ctxfg.rect(402, 10, 100, 50);
    ctxfg.lineWidth = 4;
    ctxfg.strokeStyle = 'black';
    ctxfg.stroke();
    ctxfg.font = "20pt sans-serif";
    ctxfg.fillText("Pause", 412, 45);
};
var update=function(mod){
    if(kd[38]===true||kd[87]===true){
        hero.speedy-=hero.accel*mod;
    }
    if(kd[40]===true||kd[83]===true){
        hero.speedy+=hero.accel*mod;
    }
    if(kd[37]===true||kd[65]===true){
        hero.speedx-=hero.accel*mod;
    }
    if(kd[39]===true||kd[68]===true){
        hero.speedx+=hero.accel*mod;
    }
    for(var i=0;i<30;i++){
        var a=monsters[i];
        if(a!=undefined){
            var b=hero;
            a.x=a.x+(a.speed*mod);
            if((b.x+b.size>a.x&&b.x<a.x+a.size)&&(b.y+b.size*0.5>a.y&&b.y<a.y+a.size*0.5)){
                if(a.size>=b.size){
                    reset();
                }else{
                    monsters[i]=undefined;
                    hero.size++;
                }
            }
            if(a.x>const_width||a.x<-50){
                monsters[i]=undefined;
            }
        }
    }
    var bounceMult = 1.2;
    if(hero.x>=const_width-hero.size){
        hero.x=const_width-(hero.size + 1);
        hero.speedx=hero.speedx*-bounceMult;
    }else if(hero.x<=0){
        hero.x=1;
        hero.speedx=hero.speedx*-bounceMult;
    }else if(hero.y>=const_height-hero.size*0.5){
        hero.y=const_height-(hero.size*0.5 + 1);
        hero.speedy=hero.speedy*-bounceMult;
    }else if(hero.y<=0){
        hero.y=1;
        hero.speedy=hero.speedy*-bounceMult;
    }
    hero.x+=hero.speedx;
    hero.y+=hero.speedy;
    hero.speedx=hero.speedx*hero.friction;
    hero.speedy=hero.speedy*hero.friction;
};
var render=function(){
    ctx.clearRect ( 0 , 0 , canvas.width, canvas.height );
    if(heroReady){
    	var heroDrawImage = (hero.speedx > 0)?heroImage:heroImageR;
    	ctx.drawImage(
         heroDrawImage,
         Math.round(hero.x),
         Math.round(hero.y),
         hero.size,
         hero.size*0.5
       );
    }
    for(var i=0;i<30;i++){
        if(monsters[i]!=undefined){
            var a=monsters[i];
            if(monsters[i].speed>0){
                ctx.drawImage(
                  monsterImage,
                  Math.round(a.x),
                  Math.round(a.y),
                  a.size,
                  a.size*0.5
                );
            }else{
                ctx.drawImage(
                  monsterImageR,
                  Math.round(a.x),
                  Math.round(a.y),
                  a.size,
                  a.size*0.5
                );
            }
        }
    }
};

var main=function(){
    var now=Date.now();
    var delta=now-then;
    update(delta/1000);
    render();
    then=now;
    if(!paused)requestAnimationFrame(main);
};

setInterval(function(){if(rand(1,50)==50&&!paused){createMonster();}},20)
var w = window;
requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;
var then=Date.now();
reset();
main();

$("#cfg").click(function(e){
    if((e.clientX>402&&e.clientX<502)&&(e.clientY>10&&e.clientY<60)){
        if(paused==true){
            paused=false;
            then=Date.now();
            main();
        }else{
            paused=true;
        }
    }
});
